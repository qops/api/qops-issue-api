########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref
from marshmallow import Schema, fields, ValidationError, pre_load
from issue_services import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(
        db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Issue(Base):
    __tablename__ = 'issue'

    identifier = db.Column(db.String)
    title = db.Column(db.String)
    summary = db.Column(db.String)
    description = db.Column(db.String)
    author_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    owner_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    date_found = db.Column(db.DateTime)
    date_reported = db.Column(db.DateTime)
    state_id = db.Column(db.Integer, db.ForeignKey('issue_state.id'))
    priority_id = db.Column(db.Integer, db.ForeignKey('issue_priority.id'))
    severity_id = db.Column(db.Integer, db.ForeignKey('issue_severity.id'))
    reporter_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    team_assigned_id = db.Column(db.Integer, db.ForeignKey('team.id'))
    person_assigned_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    kind_id = db.Column(db.Integer, db.ForeignKey('issue_kind.id'))
    origin_activity_id = db.Column(
        db.Integer, db.ForeignKey('issue_origin_activity.id'))
    resolution_id = db.Column(db.Integer, db.ForeignKey('issue_resolution.id'))
    workaround = db.Column(db.String(500))

    def get_next_identifier():
        return 'IS00001'


class IssueProductComponents(Base):
    __tablename__ = 'issue_product_components'

    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    component_id = db.Column(
        db.Integer, db.ForeignKey('product_components.id'))


class IssueProjectComponents(Base):
    __tablename__ = 'issue_project_components'

    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    # component_id = db.Column(db.Integer, db.ForeignKey('project_component.id'))


class ProductIssues(Base):
    __tablename__ = 'product_issues'

    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    issues = db.relationship(
        'Issue', backref=backref('product', lazy='joined'))


class ProjectIssues(Base):
    __tablename__ = 'project_issues'

    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    issues = db.relationship(
        'Issue', backref=backref('project', lazy='joined'))


class IssueConfirmation(Base):
    __tablename__ = 'issue_confirmation'

    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    #environment_id = db.Column(db.Integer, db.ForeignKey('environment.id'))
    comment = db.Column(db.String)


class IssueVote(Base):
    __tablename__ = 'issue_vote'

    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    vote = db.Column(db.Integer)
    comment = db.Column(db.String)
    timestamp = db.Column(db.DateTime)


class IssueResolution(Base):
    """Data model for issue resolution analysis."""

    date_resolved = db.Column(db.DateTime)
    resolved_in_build = db.Column(db.String)
    commit_id = db.Column(db.Integer)
    root_cause_analysis = db.Column(db.String)
    quality_assurance_focus = db.Column(db.String(500))
    resolved_by_team = db.Column(db.Integer)
    resolved_by_id = db.Column(db.Integer)


class IssueReleaseNote(Base):
    """Data model for general release notes."""

    issue_id = db.Column(db.Integer)
    internal_release_note = db.Column(db.String(500))
    all_customers_release_note = db.Column(db.String(500))


class IssueCustomerReleaseNote(Base):
    """Data model for customer-specific release notes."""

    issue_id = db.Column(db.Integer)
    customer_id = db.Column(db.Integer)
    release_note = db.Column(db.String(500))


class IssueResolvedBy(Base):
    """Data model for resolutions."""

    issue_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)


class IssueReleaseNoteReview(Base):
    """Data model for TBD."""

    issue_id = db.Column(db.Integer)
    person_id = db.Column(db.Integer)
    reviewed = db.Column(db.Boolean)


class IssueSeverity(Base):
    __tablename__ = 'issue_severity'

    name = db.Column(db.String)
    usage = db.Column(db.String)
    issues = db.relationship('Issue', backref='severity')


class IssuePriority(Base):
    __tablename__ = 'issue_priority'

    name = db.Column(db.String)
    usage = db.Column(db.String)
    issues = db.relationship('Issue', backref='priority')


class IssueState(Base):
    __tablename__ = 'issue_state'

    name = db.Column(db.String)
    usage = db.Column(db.String)
    issues = db.relationship('Issue', backref='state')


class IssueAttachment(Base):
    __tablename__ = 'issue_attachment'

    issue_id = db.Column(db.Integer)
    attachment_id = db.Column(db.Integer)


class IssueKind(Base):
    __tablename__ = 'issue_kind'

    name = db.Column(db.String())
    usage = db.Column(db.String())
    issues = db.relationship('Issue', backref='kind')


class IssueTag(Base):
    __tablename__ = 'issue_tag'


class IssueOriginActivity(Base):
    __tablename__ = 'issue_origin_activity'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)
    issues = db.relationship('Issue', backref='origin_activity')


class CustomerIssueClassification(Base):
    __tablename__ = 'customer_issue_classification'

    issue_id = db.Column(db.Integer, db.ForeignKey('issue.id'))
    customer_issue_class_id = db.Column(
        db.Integer, db.ForeignKey('customer_issue_class.id'))
    customer_issue_bucket_id = db.Column(
        db.Integer, db.ForeignKey('customer_issue_bucket.id'))
    issues = db.relationship('Issue', backref='classification')


class CustomerIssueClass(Base):
    __tablename__ = 'customer_issue_class'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)
    issues = db.relationship(
        'CustomerIssueClassification', backref='classification')


class CustomerIssueBuckets(Base):
    __tablename__ = 'customer_issue_bucket'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)
    issues = db.relationship('CustomerIssueClassification', backref='bucket')
