########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for, json, jsonify
from marshmallow import Schema, fields, ValidationError, pre_load

from ..lib import db_logger
import logging

# Import models
from .models import Issue
from .models import ProductIssues
from .models import IssueSeverity
from .models import IssuePriority
from .models import IssueState
from .models import IssueKind
from ..mod_person.models import Person
from ..mod_team.models import Team
from .models import ProjectIssues
from ..mod_product.models import Product
from ..mod_product.models import ProductComponents
from ..mod_project.models import Project
from ..mod_product.models import ProductComponentsSchema

from .forms import IssueProfileForm
from .forms import IssueResolutionForm
from .forms import IssueReleaseNoteForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_issue = Blueprint('issue',
                      __name__,
                      url_prefix='/issue')


@mod_issue.context_processor
def store():
    store_dict = {'serviceName': 'Issue',
                  'serviceDashboardUrl': url_for('issue.dashboard'),
                  'serviceBrowseUrl': url_for('issue.browse'),
                  'serviceNewUrl': url_for('issue.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_issue.route('/', methods=['GET'])
def issue():
    return render_template('issue/issue_dashboard.html')


@mod_issue.route('/dashboard', methods=['GET'])
def dashboard():
    db_logger.log(db, "Issue service loaded.")
    return render_template('issue/issue_dashboard.html')


@mod_issue.route('/browse', methods=['GET'])
def browse():
    issues = Issue.query.with_entities(Issue.id, Issue.title, Issue.date_reported, Issue.date_found, Issue.identifier, Product.name.label('product_name'), IssueState.name.label('state_id'), IssueKind.name.label('kind_id'), IssuePriority.name.label('priority_id'), IssueSeverity.name.label('severity_id'), Team.name.label('team_assigned_id')).join(
        IssueState, Issue.state_id == IssueState.id).join(IssueKind, Issue.kind_id == IssueKind.id).join(IssuePriority, Issue.priority_id == IssuePriority.id).join(IssueSeverity, Issue.severity_id == IssueSeverity.id).join(Team, Issue.team_assigned_id == Team.id).join(ProductIssues, Issue.id == ProductIssues.issue_id).join(Product, ProductIssues.product_id == Product.id).all()
    return render_template('issue/issue_browse.html', issues=issues)


@mod_issue.route('/new', methods=['GET', 'POST'])
def new():
    issue = Issue()
    form = IssueProfileForm(request.form)

    form.state_id.choices = [(s.id, s.name) for s in IssueState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.reporter_id.choices = [(r.id, r.first_name)
                                for r in Person.query.all()]
    form.kind_id.choices = [(k.id, k.name) for k in IssueKind.query.all()]
    form.priority_id.choices = [(p.id, p.name)
                                for p in IssuePriority.query.all()]
    form.severity_id.choices = [(v.id, v.name)
                                for v in IssueSeverity.query.all()]
    form.team_assigned_id.choices = [(t.id, t.name) for t in Team.query.all()]
    form.person_assigned_id.choices = [
        (p.id, p.first_name) for p in Person.query.all()]
    form.product_id.choices = [(prod.id, prod.name)
                               for prod in Product.query.all()]
    form.project_id.choices = [(proj.id, proj.name)
                               for proj in Project.query.all()]

    if request.method == 'POST':

        form.populate_obj(issue)
        issue.identifier = Issue.get_next_identifier()
        db.session.add(issue)
        db.session.commit()

        product_components = ProductComponents.query.get(issue.id)
        product_component = ProductComponents()
        product_issues = ProductIssues.query.all()
        product_issue = ProductIssues()

        # project_components = ProjectComponents.query.get(issue.id)
        # project_issues = ProjectIssues.query.all()
        # project_issue = ProjectIssues()

        if form.product_id.data:
            product_issue.issue_id = issue.id

            product_issue.product_id = form.product_id.data

            db.session.add(product_issue)
            db.session.commit()
            components_to_add = request.form.getlist('components')
            for component in components_to_add:
                product_component.issue_id = issue.id
                product_component.product_id = issue.product_id
                product_component.component_id = component['id']
                db.session.add(product_component)
                db.session.commit()

        if form.project_id.data:
            project_issue.issue_id = issue.id
            project_issue.project_id = form.project_id.data
            # project_issues.append(project_issue)
            db.session.add(project_issue)
            db.session.commit()

        return redirect(url_for('issue.browse'))
    return render_template('issue/issue_view.html', issue=issue, form=form)


@mod_issue.route('/profile', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/profile', methods=['GET', 'POST'])
def profile(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    prod_issues = ProductIssues.query.filter_by(
        issue_id=issue.id).one_or_none()
    form = IssueProfileForm(
        obj=issue, data={'product_id': prod_issues.product_id})

    #proj_issues = ProjectIssues.query.filter_by(issue_id=issue.id).one_or_none()
    #form.project_id = proj_issues.project_id

    form.state_id.choices = [(s.id, s.name) for s in IssueState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.reporter_id.choices = [(r.id, r.first_name)
                                for r in Person.query.all()]
    form.kind_id.choices = [(k.id, k.name) for k in IssueKind.query.all()]
    form.priority_id.choices = [(p.id, p.name)
                                for p in IssuePriority.query.all()]
    form.severity_id.choices = [(v.id, v.name)
                                for v in IssueSeverity.query.all()]
    form.team_assigned_id.choices = [(t.id, t.name) for t in Team.query.all()]
    form.person_assigned_id.choices = [
        (p.id, p.first_name) for p in Person.query.all()]
    form.product_id.choices = [(prod.id, prod.name)
                               for prod in Product.query.all()]
    form.project_id.choices = [(proj.id, proj.name)
                               for proj in Project.query.all()]

    if request.method == 'POST':
        form = IssueProfileForm(request.form)
        form.populate_obj(issue)
        db.session.add(issue)
        db.session.commit()

        product_issues = ProductIssues.query.all()
        product_issue = ProductIssues.query.filter_by(
            issue_id=issue.id).one_or_none()

        project_issues = ProjectIssues.query.all()
        project_issue = ProjectIssues.query.filter_by(
            issue_id=issue.id).one_or_none()

        if form.product_id.data:
            product_issue.issue_id = issue.id
            logger.debug('Issue ID: {0}'.format(issue.id))
            product_issue.product_id = form.product_id.data
            logger.debug('Product ID: {0}'.format(form.product_id.data))
            db.session.add(product_issue)
            db.session.commit()

        if form.project_id.data:
            project_issue.issue_id = issue.id
            project_issue.project_id = form.project_id.data
            db.session.add(project_issue)
            db.session.commit()

        return redirect(url_for('issue.browse'))
    return render_template('issue/issue_view.html', issue=issue, form=form)


@mod_issue.route('/view', methods=['GET', 'POST'])
@mod_issue.route('/view/<int:issue_id>/view', methods=['GET', 'POST'])
def issue_view(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    #prod_issues = ProductIssues.query.filter_by(
        #issue_id=issue.id).one_or_none()
    form = IssueProfileForm(
        obj=issue)

    #proj_issues = ProjectIssues.query.filter_by(issue_id=issue.id).one_or_none()
    #form.project_id = proj_issues.project_id

    form.state_id.choices = [(s.id, s.name) for s in IssueState.query.all()]
    form.author_id.choices = [(a.id, a.first_name) for a in Person.query.all()]
    form.owner_id.choices = [(o.id, o.first_name) for o in Person.query.all()]
    form.reporter_id.choices = [(r.id, r.first_name)
                                for r in Person.query.all()]
    form.kind_id.choices = [(k.id, k.name) for k in IssueKind.query.all()]
    form.priority_id.choices = [(p.id, p.name)
                                for p in IssuePriority.query.all()]
    form.severity_id.choices = [(v.id, v.name)
                                for v in IssueSeverity.query.all()]
    form.team_assigned_id.choices = [(t.id, t.name) for t in Team.query.all()]
    form.person_assigned_id.choices = [
        (p.id, p.first_name) for p in Person.query.all()]
    form.product_id.choices = [(prod.id, prod.name)
                               for prod in Product.query.all()]
    form.project_id.choices = [(proj.id, proj.name)
                               for proj in Project.query.all()]

    if request.method == 'POST':
        form = IssueProfileForm(request.form)
        form.populate_obj(issue)
        db.session.add(issue)
        db.session.commit()

        product_issues = ProductIssues.query.all()
        product_issue = ProductIssues.query.filter_by(
            issue_id=issue.id).one_or_none()

        project_issues = ProjectIssues.query.all()
        project_issue = ProjectIssues.query.filter_by(
            issue_id=issue.id).one_or_none()

        if form.product_id.data:
            product_issue.issue_id = issue.id
            logger.debug('Issue ID: {0}'.format(issue.id))
            product_issue.product_id = form.product_id.data
            logger.debug('Product ID: {0}'.format(form.product_id.data))
            db.session.add(product_issue)
            db.session.commit()

        if form.project_id.data:
            project_issue.issue_id = issue.id
            project_issue.project_id = form.project_id.data
            db.session.add(project_issue)
            db.session.commit()

        return redirect(url_for('issue.browse'))
    return render_template('issue/issue_view.html', issue=issue, form=form)


@mod_issue.route('/profile/dashboard', methods=['GET'])
@mod_issue.route('/profile/<int:issue_id>/dashboard', methods=['GET'])
def issue_dashboard(issue_id=None):
    if issue_id is not None:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_view.html', issue=issue)


@mod_issue.route('/profile/tasks', methods=['GET'])
@mod_issue.route('/profile/<int:issue_id>/tasks', methods=['GET'])
def issue_tasks(issue_id=None):
    issue = Issue.query.get(issue_id)
    return render_template('issue/issue_single_tasks.html', issue=issue)


@mod_issue.route('/profile/communication', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/communication', methods=['GET'])
def issue_communication(issue_id=None):
    issue = Issue.query.get(issue_id)
    return render_template('issue/issue_single_communication.html',
                           issue=issue)


@mod_issue.route('/profile/documents', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/documents', methods=['GET'])
def issue_documents(issue_id=None):
    issue = Issue.query.get(issue_id)
    return render_template('issue/issue_single_documents.html', issue=issue)


@mod_issue.route('/profile/builds', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/builds', methods=['GET'])
def issue_builds(issue_id=None):
    issue = Issue.query.get(issue_id)
    return render_template('issue/issue_single_builds.html', issue=issue)


@mod_issue.route('/profile/requirements', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/requirements', methods=['GET'])
def issue_requirements(issue_id=None):
    issue = Issue.query.get(issue_id)
    return render_template('issue/issue_single_requirements.html', issue=issue)


@mod_issue.route('/profile/customers', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/customers', methods=['GET'])
def issue_customers(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_single_customers.html', issue=issue)


@mod_issue.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/test-cases', methods=['GET'])
def issue_test_cases(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_single_test_cases.html', issue=issue)


@mod_issue.route('/profile/locations', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/locations', methods=['GET'])
def issue_locations(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_single_locations.html', issue=issue)


@mod_issue.route('/profile/buckets', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/buckets', methods=['GET', 'POST'])
def issue_buckets(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_single_buckets.html', issue=issue)


@mod_issue.route('/profile/products', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/products', methods=['GET', 'POST'])
def issue_products(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_single_products.html', issue=issue)


@mod_issue.route('/profile/votes', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/votes', methods=['GET', 'POST'])
def issue_votes(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    return render_template('issue/issue_single_votes.html', issue=issue)


@mod_issue.route('/profile/resolution', methods=['GET', 'POST'])
@mod_issue.route('/profile/<int:issue_id>/resolution', methods=['GET', 'POST'])
def issue_resolution(issue_id=None):
    if issue_id:
        issue = Issue.query.get(issue_id)
    else:
        issue = None
    rel_note_form = IssueReleaseNoteForm()
    res_form = IssueResolutionForm()
    return render_template('issue/issue_single_resolution.html',
                           issue=issue, rel_note_form=rel_note_form,
                           res_form=res_form)


@mod_issue.route('/get_product_components/<product_id>', methods=['GET'])
def get_product_components(product_id):
    logger = logging.getLogger('qops')
    logger.debug('In /get_product_components')

    l = []
    product_components = ProductComponents.query.filter_by(product_id=product_id)
    for pc in product_components:
        l.append({"id": pc.id, "component": pc.name})
    sj = jsonify({"components": l})
    logger.debug(str(sj))
    return sj
